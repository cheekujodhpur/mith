/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>*suspense music plays in the background*<br />And here we are, ready to catch Vladimir in his acts. But what does Freddie see on his arrival? His nemesis has left out clues for him to crack. Vladimir likes to play too. Crack each of them, and Freddie gets closer to him...</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "c97bb2385b52c5e31eaeab776d1cb532.png"
