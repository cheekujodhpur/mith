/*some central functions*/


function resizeIframe(obj)
{
	obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

function loadFrame(i)
{
	$("#frame_image").attr("src",image_urls[i]);
	$("#frame_text").html(content_texts[i]);	
}

function nextFrame()
{
	$("#help_frame").hide();
	if(current_frame<number_of_frames)
	{	
		loadFrame(current_frame);
		current_frame++;
	}
	else
	{
		$("#question_frame").show();
		$("#frame_container").hide();
		if(question_image!='')
			$("#question_image").attr("src",question_image);
		$("input[name='answer']").focus();
	}
}

function loadHelp()
{
	$("#question_frame").hide();
	$("#frame_container").hide();
	$("#help_frame").show();
	$("#help_frame").find("iframe").css("height","auto");
	$("#help_frame").find("iframe").attr("src","/mith/rules.php");
}

function loadLeaderboard()
{
	$("#question_frame").hide();
	$("#frame_container").hide();
	$("#help_frame").show();
	$("#help_frame").find("iframe").css("height","auto");
	$("#help_frame").find("iframe").attr("src","/mith/lb.php");
}
function loadMap()
{
	$("#question_frame").hide();
	$("#frame_container").hide();
	$("#help_frame").show();
	$("#help_frame").find("iframe").css("height","auto");
	$("#help_frame").find("iframe").attr("src","map.php");
}

$(document).ready(function(){
	nextFrame();
})
