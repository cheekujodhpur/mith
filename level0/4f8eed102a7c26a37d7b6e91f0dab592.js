/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>He woke up to find himself in the wilderness of Kau Forest Reserve. The melody of birds enhanced his newfound love for breathing. He had barely survived.</p><p>\"Eh! No biggie. Let's keep moving\" he told himself. It'd all be simple only if he could figure out the way to the nearest airport. But his enthusiasm faded a bit when he found his map torn in pieces from the crash.</p><p>You gotta help him. Who else will?</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = ""
