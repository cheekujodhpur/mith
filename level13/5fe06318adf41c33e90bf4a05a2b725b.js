/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>Freddie reaches the Island of Maui and the first thing to obtain is the Gift of Water.<br /> He shall do two tasks to get through this.</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "1457ee6f13b3def04c05899c418bf218.png"
