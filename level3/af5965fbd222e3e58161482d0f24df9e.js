/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>Mr. Lavalava is impressed by your intellect. He gives you a piece of rock, which makes Freddie go 'meh' but he appreciates the gesture. Only later does he notice that the piece of rock seems to be a broken part of a tablet. It has all sorts of inscriptions in wiggly language. </p><p>He already has other stuff on his mind to help him pay less attention to it though. He tosses it in his backpack and moves on.</p><p>Humpty Dumpty sat on a wall, and Freddie suffered from the Great Fall. Who can help him if not his guardian angel?</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "a6c987deabf88ce8b117e39cc686f436.png"
