/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>You are in the city of James Cook, the second in command to the expedition which measured the Venus transit. Charles Green, the astronomer responsible for the great contribution to international scientific collaboration and writing history died of Malaria soon after his work was finished. In his memory, this weird city lies.</p><p>The gatekeepers don't like you much, but the will give you passage only if you solve a wait-for-it RIDDLE :D </p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "ead541771a603f05fa0020a03c5cff52.png"
