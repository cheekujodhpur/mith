/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>Now that he has the map, he starts moving northeast towards Hilo Airport until he encounters a volcano in his way. There is no sound of trouble in the setup for anyone who shares the love of a little adventure like our hero...ahem...protagonist. But when a smoking hot man, and we mean it in a very literal sense, pops out of the volcano, you start reconsidering your fear triggers.</p><p>He is not harmful, doesn't threaten Freddie one bit but doesn't stop talking at all. He offers to not let you pass until you tell him what he's thinking about.</p> "];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "0e95c20247df45cc88859cd3b37e5ca3.png"
