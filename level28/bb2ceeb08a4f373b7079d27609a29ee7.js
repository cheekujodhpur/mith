/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>You stand at the gates of MI and the guards are rogue. Just the last riddle to break it up, and then you're in...</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "ae79e22874923d3d2fc0c89ba8b2e33c.png"
