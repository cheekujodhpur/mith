/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>Master Hebbar directs Freddie to find each of the missing pieces of the tablet which shall reveal before them the Grand Prophecy. He then embarks to the next volcano at Mauna Loa to meet Mr. Magmagma, the brother of Mr. Lavalava who shall give him the next piece of the tablet.</p><p>And Freddie reaches a river. But will the fishy fish let him pass without a riddle? So much better, so much fun.</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "b20852d652514422e93cd26570d5bb82.png"
