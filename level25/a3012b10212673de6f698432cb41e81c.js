/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>Something happened. We didn't tell you about it. Freddie got very close to catching Vladimir. He's gotten weak and is fleeing at the airport. But Freddie looks like he's chilling, ain't he? Oh c'mon! Haven't you learnt your lesson yet? He is always in spy mode, checking out umm...people that pass by. It's the final problem. Do it fast.</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "b67f8c21009094aff8f8a906376833c6.png"
