/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>Phew! Vladimir escaped. But you should've expected that. Did you really not know what the 'Final Problem' was? Anyway, Freddie did manage to find his notebook. He forgot it in the toilet. Stupid little villain. He's weak, so not a threat anymore. But he finds about his plans to destroy MI already in motion.</p><p>If you were smarty enough pants to get here, you should be able to guess that the only one who is legen-wait-for-it-dary enough to break them is Fredide. </p><p>And hence he finds himself chasing a truck supposed to get MI Ambiance supplies. It seems to be hijacked, and he is the one being chased now.</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = ""
