/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>No matter how important your work is, even as important as saving MI, if you stop by a beach, you chill! And that's what Freddie's doing, or so it seems. He's secretly looking for Vladimir with his X-ray vision. Isn't there more you could do on a beach with X-ray vision? Maybe I should shut up and leave that to your imagination.</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "79c6caed7a331d761c8a1f5fd6827fea.png"
