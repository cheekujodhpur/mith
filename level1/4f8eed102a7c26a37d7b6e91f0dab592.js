/*Holds the content of the page*/

//hold number of frames
number_of_frames = 1;
//init to start on load
current_frame = 0;

image_urls = ["1.jpg"];
content_texts = ["<p>An ancient, though beautifully designed image blocks your way. Can you beat the might of IDC here?</p>"];

function validateContent()
{
	//validates number of frames
	return image_urls.length==number_of_frames && content_texts.length;
}

question_image = "dance.png"
